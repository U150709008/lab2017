
public class Rectangle {

	
	int sideA;
	int sideB;
	
	public int area(){
		int result = sideA * sideB;
		return result;
		
	}
	
	public int perimeter(){
		int result = 2*(sideA+sideB);
		return result;
	}
}
