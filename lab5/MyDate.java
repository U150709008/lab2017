public class MyDate {

    int day;
    int month;
    int year;

	int[] maxDays = { 31,29,31,30,31,30,31,31,30,31,30,31 } ;

	public MyDate(int day, int month, int year) {

        this.day = day;
        this.month = month-1;
        this.year = year;
	

	}



	public void incrementDay() {
		
		day++;
		if (day > maxDays[month] || (inLeapYear()==false && (month==1 && day==29)) ){
			day = 1;
			incrementMonth(); // it will adjust the month and year //
			}

	}

	public boolean inLeapYear(){
		if (year % 4 == 0){
			return true;
		}else{
		return  false;
		}

	}



	public void decrementDay() {

	day--;
	if (day == 0 ) {
		day = 31;
		decrementMonth(); // will also assign the max say of the resulting month of the day //
		}
		
	}
	


	public void incrementDay(int i) {

		while (i > 0){
		incrementDay();
		i--;
		}
	}


	
	public void decrementDay(int i) {

		while (i > 0){
		decrementDay();
		i--;
		}	
	}




	public void incrementMonth(int i) {

		int newMonth = (month +i) % 12;
		int incrementYear =0;
		if (newMonth < 0){
			newMonth+=12;
			incrementYear--;
			}

		incrementYear += (month +i) /12 ;
		if (day > maxDays[newMonth]){
			day = maxDays[newMonth];
	
		}else if ( month==1 && day==29 && !inLeapYear()){
			day = 28;
		}
		month = newMonth;
		year += incrementYear;	
	}


	
	
	public void incrementMonth() {
		incrementMonth(1);
		
	}



	public void decrementMonth(int i) {
		incrementMonth(-i);
		
	}


	
	public void decrementMonth() {
		incrementMonth(-1);
		
	}


	
	public void incrementYear(int i) {

		year+= i;
	if (month ==1 && day== 29 && (!inLeapYear())){
	day=28;
	}
			
	}

	public void incrementYear() {
	incrementYear(1);	
	}

	public void decrementYear() {
	incrementYear(-1);
	}


	public void decrementYear(int i) {
		incrementYear(-i);
	}


	public boolean isBefore(MyDate anotherDate) {

		String thisDate = toString().replaceAll("-","");
		
	String antString = anotherDate.toString().replace("-","");
	
	if(Integer.parseInt(thisDate) <Integer.parseInt(antString)){

	return true;
	}else{
		return false;
	}
	}
	
	
	

	public int dayDifference(MyDate D) {
	MyDate date = new MyDate(day,(month+1),year);
	
            int d= D.day;
            int m= (D.month+1);
            int y= D.year;

	MyDate otherDate = new MyDate(d,m,y);
	
	int diff =0;	
	
	if ( date.isBefore(otherDate)==true){
		while (date.isBefore(otherDate)){
			diff = diff+1;
			otherDate.decrementDay();
			
		
		}
	}else{
			while (date.isAfter(otherDate)==true){	
				diff = diff+1;
			otherDate.incrementDay();
			
			
		}
	}
	return diff-1;
	}

	
	
	
	public boolean isAfter(MyDate anotherDate) {
		String thisDate = toString().replaceAll("-","");
	String antString = anotherDate.toString().replace("-","");

	if(Integer.parseInt(thisDate) <Integer.parseInt(antString)){

		return false;
	}else {
		return true;
	}
	}


	
	public String toString(){

		return year +"-"+ ((month +1)<10 ? "0" : "") + (month+1) +"-"+( day<10 ? "0" : "") + day;
	}
	
	}