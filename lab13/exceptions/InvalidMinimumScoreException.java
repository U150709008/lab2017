package exceptions;

public class InvalidMinimumScoreException extends Exception {

	public InvalidMinimumScoreException() {
	}

	public InvalidMinimumScoreException(String message) {
		super(message);
	}

	public InvalidMinimumScoreException(Throwable cause) {
		super(cause);
	}

	public InvalidMinimumScoreException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidMinimumScoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}