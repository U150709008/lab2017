public class Fibonacci {

	public static void main(String[] args){

		int givennum = Integer.parseInt(args[0]);


		if (givennum==0){
			System.out.println(givennum);
		} else if (givennum==1){
			System.out.println("0, 1, 1");			

		} else if (givennum>=2){
			System.out.print("0, 1, 1, 2");

			int t= 1;
			int storingnum = 2;
			int newnum = 0;		
			while ( storingnum+t<=givennum ) {

				newnum = storingnum + t;
				System.out.print(", " + newnum);

				t= storingnum;
				storingnum = newnum;
			}

		System.out.println();
		}
	}
}



